<?php
/**
 * m210824_145518_archive_location_module_create_table_building
 * 
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)811-2540-432
 * @copyright Copyright (c) 2021 OMMU (www.ommu.id)
 * @created date 24 August 2021, 14:56 WIB
 * @link https://bitbucket.org/ommu/archive-location
 *
 */

use Yii;
use yii\db\Schema;

class m210824_145518_archive_location_module_create_table_building extends \yii\db\Migration
{
	public function up()
	{
		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_location_building';
		if (!Yii::$app->db->getTableSchema($tableName, true)) {
			$this->createTable($tableName, [
				'id' => Schema::TYPE_INTEGER . '(11) UNSIGNED NOT NULL AUTO_INCREMENT',
				'publish' => Schema::TYPE_TINYINT . '(1) NOT NULL DEFAULT \'1\'',
				'parent_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'type' => Schema::TYPE_STRING,
				'location_name' => Schema::TYPE_STRING . '(128) NOT NULL',
				'location_desc' => Schema::TYPE_TEXT . ' NOT NULL',
				'creation_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT \'trigger\'',
				'creation_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'modified_date' => Schema::TYPE_TIMESTAMP . ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT \'trigger,on_update\'',
				'modified_id' => Schema::TYPE_INTEGER . '(11) UNSIGNED',
				'updated_date' => Schema::TYPE_DATETIME . ' NOT NULL DEFAULT \'0000-00-00 00:00:00\' COMMENT \'trigger\'',
				'PRIMARY KEY ([[id]])',
			], $tableOptions);

			$this->createIndex(
				'publishWithParentAndType',
				$tableName,
				['publish', 'parent_id', 'type']
			);

			$this->createIndex(
				'publishWithType',
				$tableName,
				['publish', 'type']
			);

			$this->createIndex(
				'publishWithLocationName',
				$tableName,
				['publish', 'location_name']
			);

			$this->createIndex(
				'location_name',
				$tableName,
				['location_name']
			);
		}
	}

	public function down()
	{
		$tableName = Yii::$app->db->tablePrefix . 'ommu_archive_location_building';
		$this->dropTable($tableName);
	}
}
